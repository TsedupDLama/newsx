class RouteName {
  // This file contains all the routing AppConstants used within the app
  static const String splashRoute = '/';
  static const String home = '/homeScreen';
}
