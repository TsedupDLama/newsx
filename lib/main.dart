import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'app_config/app_colors.dart';
import 'app_config/app_theme.dart';
import 'routes/route_generator.dart';
import 'routes/route_name.dart';

void main() {
  runApp(
    DevicePreview(
      enabled: true,
      builder: (context) => MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
          statusBarColor: AppColors.colorBlack(0.5)
      ),
      child: MaterialApp(
        locale: DevicePreview.locale(context),
        onGenerateRoute: RouteGenerator.generateRoute,
        builder: DevicePreview.appBuilder,
        initialRoute: RouteName.home,
        theme: AppTheme.appTheme(),
      ),

    );
  }
}
