import 'package:flutter/material.dart';
import 'package:newsx/views/splashscreen.dart';

import 'route_name.dart';




class RouteGenerator {
  // Getting arguments passed in while calling Navigator.pushNamed
  static Route<dynamic> generateRoute(RouteSettings settings) {
    // var args = settings.arguments; //this will holds our constructor parameter to be sent on another page/screen
    switch (settings.name) {
      case RouteName.splashRoute:
        return MaterialPageRoute(
            settings: settings, builder: (_) => SplashScreen());

      default:
        // If there is no such named route in the switch statement, e.g. /third
        return MaterialPageRoute(
            builder: (_) => const Scaffold(
                body: SafeArea(child: Center(child: Text('No routes found')))));
    }
  }
}
